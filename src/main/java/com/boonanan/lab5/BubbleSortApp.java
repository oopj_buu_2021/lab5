package com.boonanan.lab5;

public class BubbleSortApp {
    public static void main(String[] args) {
        int arr[] = new int[1000];
        randomarr(arr);
        print(arr);
        Bubblesort(arr);
        print(arr);
    }

    public static void Swap(int[] arr, int first, int second) {
        int temp = arr[first];
        arr[first] = arr[second];
        arr[second] = temp;
    }

    public static void Bubble(int[] arr, int first, int second) {
        for (int i = first; i < second; i++) {
            if (arr[i] > arr[i + 1]) {
                Swap(arr, i, i + 1);
            }
        }
    }

    public static void Bubblesort(int[] arr) {
        for (int i = arr.length - 1; i > 0; i--) {
            Bubble(arr, 0, i);
        }
    }

    public static void print(int[] arr) {
        for (int a : arr) {
            System.out.print(a + " ");
        }
        System.out.println();
    }

    public static void randomarr(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) (Math.random() * 10000);
        }
    }

}
