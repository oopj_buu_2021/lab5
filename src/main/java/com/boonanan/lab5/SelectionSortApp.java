package com.boonanan.lab5;

public class SelectionSortApp {
    public static void main(String[] args) {
        int arr[] = new int[1000];
        randomarr(arr);
        print(arr);
        Selectionsort(arr);
        print(arr);
    }

    public static void print(int[] arr) {
        for (int a : arr) {
            System.out.print(a + " ");
        }
        System.out.println();
    }

    public static void randomarr(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int)(Math.random()*10000);
        }
    }

    public static int Findminindex(int arr[], int pos) {
        int minIndex = pos;
        for (int i = pos; i < arr.length; i++) {
            if (arr[minIndex] > arr[i]) {
                minIndex = i;

            }
        }
        return minIndex;
    }

    public static void Swap(int[] arr, int first, int second) {
        int temp = arr[first];
        arr[first] = arr[second];
        arr[second] = temp;
    }

    public static void Selectionsort(int[] arr) {
        int minindex;
        for (int pos = 0; pos < arr.length - 1; pos++) {
            minindex = Findminindex(arr, pos);
            Swap(arr, minindex, pos);
        }
    }
}
