package com.boonanan.lab5;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class AppTest {
    @Test
    public void should1Add1To2() {
        int result = App.add(1, 1);
        assertEquals(2, result);
    }

    @Test
    public void should2Add1To3() {
        int result = App.add(2, 1);
        assertEquals(3, result);
    }

    @Test
    public void should1Ne1To0() {
        int result = App.add(-1, 0);
        assertEquals(-1, result);
    }
}
