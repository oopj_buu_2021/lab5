package com.boonanan.lab5;

import static org.junit.Assert.assertArrayEquals;
import org.junit.Test;

public class BubbleSortAppTest {
    @Test
    public void Bubblesort1() {
        int arr[] = { 5, 4, 3, 2, 1 };
        int sorted[] = { 4, 3, 2, 1, 5 };
        int first = 0, second = 4;
        BubbleSortApp.Bubble(arr, first, second);
        assertArrayEquals(sorted, arr);

    }

    @Test
    public void Bubblesort2() {
        int arr[] = { 4, 3, 2, 1, 5 };
        int sorted[] = { 3, 2, 1, 4, 5 };
        int first = 0, second = 3;
        BubbleSortApp.Bubble(arr, first, second);
        assertArrayEquals(sorted, arr);

    }

    @Test
    public void Bubblesort3() {
        int arr[] = { 3, 2, 1, 4, 5 };
        int sorted[] = { 2, 1, 3, 4, 5 };
        int first = 0, second = 2;
        BubbleSortApp.Bubble(arr, first, second);
        assertArrayEquals(sorted, arr);

    }

    @Test
    public void Bubblesort4() {
        int arr[] = { 2, 1, 3, 4, 5 };
        int sorted[] = { 1, 2, 3, 4, 5 };
        int first = 0, second = 1;
        BubbleSortApp.Bubble(arr, first, second);
        assertArrayEquals(sorted, arr);

    }

    @Test
    public void Bubblesortarr1() {
        int arr[] = { 5, 4, 3, 2, 1 };
        int sorted[] = { 1, 2, 3, 4, 5 };
        BubbleSortApp.Bubblesort(arr);
        assertArrayEquals(sorted, arr);

    }
    @Test
    public void Bubblesortarr2() {
        int arr[] = { 10, 9, 8, 7, 6, 5, 4, 3, 2, 1 };
        int sorted[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        BubbleSortApp.Bubblesort(arr);
        assertArrayEquals(sorted, arr);

    }
}
