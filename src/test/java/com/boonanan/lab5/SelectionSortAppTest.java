package com.boonanan.lab5;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class SelectionSortAppTest {
    @Test
    public void MinIndex1() {
        int arr[] = { 5, 4, 3, 2, 1 };
        int pos = 0;
        int minindex = SelectionSortApp.Findminindex(arr, pos);
        assertEquals(4, minindex);

    }

    @Test
    public void MinIndex2() {
        int arr[] = { 1, 4, 3, 2, 5 };
        int pos = 1;
        int minindex = SelectionSortApp.Findminindex(arr, pos);
        assertEquals(3, minindex);

    }

    @Test
    public void MinIndex3() {
        int arr[] = { 1, 2, 3, 4, 5 };
        int pos = 2;
        int minindex = SelectionSortApp.Findminindex(arr, pos);
        assertEquals(2, minindex);

    }

    @Test
    public void MinIndex4() {
        int arr[] = { 1, 1, 1, 1, 1, 0, 1, 1 };
        int pos = 0;
        int minindex = SelectionSortApp.Findminindex(arr, pos);
        assertEquals(5, minindex);

    }

    @Test
    public void Swap1() {
        int arr[] = { 5, 4, 3, 2, 1 };
        int sorted[] = { 1, 4, 3, 2, 5 };
        int first = 0, second = 4;
        SelectionSortApp.Swap(arr, first, second);
        assertArrayEquals(sorted, arr);

    }

    @Test
    public void Swap2() {
        int arr[] = { 5, 4, 3, 2, 1 };
        int sorted[] = { 5, 4, 3, 2, 1 };
        int first = 0, second = 0;
        SelectionSortApp.Swap(arr, first, second);
        assertArrayEquals(sorted, arr);

    }

    @Test
    public void Selectionsort1() {
        int arr[] = { 5, 4, 3, 2, 1 };
        int sorted[] = { 1, 2, 3, 4, 5 };
        SelectionSortApp.Selectionsort(arr);
        assertArrayEquals(sorted, arr);

    }

    @Test
    public void Selectionsort2() {
        int arr[] = { 10, 9, 8, 7, 6, 5, 4, 3, 2, 1 };
        int sorted[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        SelectionSortApp.Selectionsort(arr);
        assertArrayEquals(sorted, arr);

    }

    @Test
    public void Selectionsort3() {
        int arr[] = { 1,1,1,1,1 };
        int sorted[] = { 1,1,1,1,1 };
        SelectionSortApp.Selectionsort(arr);
        assertArrayEquals(sorted, arr);

    }

}
